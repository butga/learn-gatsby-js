/* Default bukan komponen (statis) */
/*
    import React from "react"

    export default function Header() {
    return <h1>This is a header.</h1>
    }
*/

import React from "react"

export default function Header(props) {
  return <h1>{props.headerText}</h1>
}